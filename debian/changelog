flask-migrate (4.1.0-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Remove myself from Uploaders.

  [ Carsten Schoenert ]
  * [6d8d2af] New upstream version 4.1.0
  * [4075334] d/control: Increase Standards-Version to 4.7.0
    No further changes needed.
  * [3e86e4a] d/copyright: Update copyright year data

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 15 Jan 2025 16:19:22 +0200

flask-migrate (4.0.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.0.7

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 15 Mar 2024 19:35:49 +0100

flask-migrate (4.0.5-1) unstable; urgency=medium

  * [e3e4838] d/watch: Switch to git mode
  * [9593600] New upstream version 4.0.5
  * [4ba2a53] Rebuild patch queue from patch-queue branch
    Dropped patch:
    debian-hacks/db-check-Revert-upstream-functionality-to-check-a-db.patch
    Not needed any more as we are packaging for unstable/testing and alembic
    is "new" enough.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 12 Jan 2024 19:12:08 +0100

flask-migrate (4.0.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.0.4
  * Rebuild patch queue from patch-queu branch
    Added patch:
    debian-hacks/db-check-Revert-upstream-functionality-to-check-a-db.patch
    Bookworm isn't shipping alembic >= 1.9.0 which is supporting the option
    to check for a required migration of a database.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 19 Feb 2023 08:43:52 +0100

flask-migrate (4.0.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.0.1
  * d/copyright: Update copyright year data
  * d/u/metadata: Add field Documentation

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 09 Jan 2023 23:27:57 +0100

flask-migrate (4.0.0-2) unstable; urgency=medium

  * Team upload.
  * d/control: Increase Standards-Version to 4.6.2

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 18 Dec 2022 17:07:50 +0100

flask-migrate (4.0.0-1) unstable; urgency=medium

  * Team upload.
  * d/gbp.conf: Add a basic default configuration
  * New upstream version 4.0.0
  * Rebuild patch queue from patch-queu branch
    Added patch:
    docs-Disable-GitHub-banner-and-button.patch
    Updated patch:
    run-sys.executable-m-flask-not-the-global-flask.patch
  * d/control: Update build deps, add BuildProfileSpecs
  * d/control: Small update to long description
  * Rename d/docs -> d/python3-flask-migrate.docs
  * Adding new binary package flask-migrate-doc
  * d/copyright: Update copyright holders and year data
  * d/control: Increase Standards-Version to 4.6.1
  * autopkgtest: Add upstream tests as test setup
  * d/dontrol: Set Homepage to current GitHub project site

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 15 Dec 2022 18:42:18 +0100

flask-migrate (3.1.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + python3-flask-migrate: Drop versioned constraint on alembic in Depends.

  [ Sandro Tosi ]
  * New upstream release; Closes: #997349
  * debian/watch
    - track github releases
  * debian/rules
    - update location of template
    - set PYTHONPATH to run tests
  * debian/patches/run-sys.executable-m-flask-not-the-global-flask.patch
    - refresh patch
  * debian/control
    - drop python3-flask-script from b-d, no longer needed

 -- Sandro Tosi <morph@debian.org>  Mon, 13 Jun 2022 17:17:53 -0400

flask-migrate (2.6.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * New upstream release.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.
  * Bump debhelper compat level to 13.
  * Bump Standards-Version to 4.5.1.
  * d/watch: Bump version to 4.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Ondřej Nový <onovy@debian.org>  Wed, 27 Jan 2021 17:54:38 +0100

flask-migrate (2.5.3-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.5.0.
  * d/patches:
    - Drop use-sys-executable.patch, applied upstream
    - Rebased other
  * Rules-Requires-Root: no.

 -- Ondřej Nový <onovy@debian.org>  Mon, 16 Mar 2020 11:54:47 +0100

flask-migrate (2.5.2-1) unstable; urgency=medium

  * New upstream release.
  * Rebase patches.
  * Rename "hash" patch to run-sys.executable-m-flask-not-the-global-flask.

 -- Ondřej Nový <onovy@debian.org>  Fri, 13 Sep 2019 13:37:58 +0200

flask-migrate (2.1.1-3) unstable; urgency=medium

  [ Thomas Bechtold ]
  * Remove myself from Uploaders (Closes: #892669)

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control:
    - Remove ancient X-Python-Version field
    - Remove ancient X-Python3-Version field
    - Add myself as uploader
    - Set Vcs-* to salsa.debian.org
  * d/patches/use-sys-executable.patch: Fix Python3 support in tests
  * Drop Python 2 support
  * Bump debhelper compat level to 12 and use debhelper-compat
  * Bump standards version to 4.4.0 (no changes)
  * Enable autopkgtest-pkg-python testsuite

 -- Ondřej Nový <onovy@debian.org>  Tue, 16 Jul 2019 02:24:56 +0200

flask-migrate (2.1.1-2) unstable; urgency=medium

  * Team upload.
  * debian/rules: Set LC_ALL=C.UTF-8 to make "Click" happy under python3.
    (Closes: #883732)

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 07 Dec 2017 13:23:17 +0100

flask-migrate (2.1.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    + Fixes FAIL: test_multidb_migrate_upgrade. (Closes: #880893)
  * Add Michael Hudson-Doyle's patch to fix failure with mixed python2/3
    versions. Thanks! (Closes: #866565)

 -- Christoph Berg <myon@debian.org>  Wed, 06 Dec 2017 21:17:27 +0100

flask-migrate (1.7.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Provide python 3 package
  * Switch to pypi.debian.net for watch file
  * Add missing Vcs fields
  * Pump up s-d version to 3.9.7, no changes needed
  * Run wrap-and-sort

 -- Orestis Ioannou <orestis@oioannou.com>  Thu, 18 Feb 2016 13:19:14 +0100

flask-migrate (1.2.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Bechtold <toabctl@debian.org>  Wed, 08 Jan 2014 07:04:30 +0100

flask-migrate (1.1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/rules: enable testsuite. tests/ dir is now included in upstream
    tarball from pypi.
  * debian/control: Add python-flask-script, python-flask-sqlalchemy and
    alembic to Build-Depends. Needed for testsuite.

 -- Thomas Bechtold <toabctl@debian.org>  Wed, 08 Jan 2014 06:38:25 +0100

flask-migrate (1.1.0-1) unstable; urgency=medium

  * Initial release (Closes: #734040).

 -- Thomas Bechtold <toabctl@debian.org>  Fri, 03 Jan 2014 11:05:04 +0100
